import React, { Component } from 'react';
import { Alert, AppState, BackHandler, View, Text } from 'react-native';
import {
  BarcodeCapture,
  BarcodeCaptureOverlay,
  BarcodeCaptureSettings,
  Symbology,
  SymbologyDescription,
} from 'scandit-react-native-datacapture-barcode';
import {
  Camera,
  CameraSettings,
  DataCaptureContext,
  DataCaptureView,
  FrameSourceState,
  RectangularViewfinder,
  VideoResolution,
} from 'scandit-react-native-datacapture-core';

import { requestCameraPermissionsIfNeeded } from './camera-permission-handler';

//export class App extends Component {
export default class App  extends Component {

  constructor() {
    super();

    // Create data capture context using your license key.
    this.dataCaptureContext = DataCaptureContext.forLicenseKey('AY8//zXTKpXoFAi+9CRf9zE00QOpMvmJTnGue7MOirrLdCRLqWtL7XxzQb27ZhQvPHBRqGdw8acNRL3v4gmyABRaBU/RbjaZjDvTpH0teAygarpZ72dWkbZOA0kNHoffgmPlUs9W7nZHUvrhMmjrs3hi7B4aUR+y0naqtOFMMpDfevyoICRTUQhfOuLOfJw/T0CIB1d4yBnBZ9AcUU9NHNpTF7s+Xdo4wFV30tV3FxImV1oYTk/K/GhK2ORAS2W7kEq+OMlY/Pb7eXe1XkG8O7gm36AueWEex3wIpatEDG2eVtRZ7mGDoupqpMMwU1AYDlxM1mR9FjAiVy7b+HslhlZgMEdJUHKYz2fXHC4QWcKZRBly+UHXzwJtE3tsd9f7FXp/tRxLUK+UdylvdXdxaztvPFClYwyn1G8y9hFmIlJpRmw9LnBEs6p0ngGfWMLM/0nJfzpAEU0wJsnsCG1u4LhFhFo3a4oi4kMWU2x8Nqe1a1j0eC6cNTNhQ6u0fwFjazarzW4U3ht9ONyhpwe3Dkt4ZsXLINtZXMCY+3d9OL/9l+pWU2XT+DCJmntz7Va/mbU4Qwk3pE97C8SsjAFjaBS1/7wWaf0ZsXgCp6c+86K8Ns+pJxTMwaEfHzL+826764SyiYVMgNeWHLEuc2+Ubk1yCoVIJx9/ri09S5yE94zQ9Tie94kTTCXWMDhBJTmH8jE9wICUvq+K6Ek8Vi09L1s0YDmYFSPyQqmV0RcV9JSogu2MncGqiwHNxaVjSB7GxXzkjrnvxw+xCfwImF/iOBViEz/mjZNPv7eH24/V2SVNhpCaxr3cZxaR6TVUkSiwRUvvaEOjNf1ehsemQGFJLOxfrIM1k3yx/uVDjvZLaAp0g5pnzZKfQl/zfPYpFd1GK7YUC9+mXOrKmrdptGzlPW/YV3VbqP8BfcmNWCm4eV5FbAqfZGkPvBz3t5M0wlAmGNuZ7dxM4YPRvoCTPwTIYGtGxpu+dtWtU08ABtbErbRZsgAXx3D0UdDIi41ERHysxeuGhO9aHGznVhjPBt+EWUr4PiaLfXbid0qHl+PjuKCD6L09j/22oeHdaOlOtf6hX2em640xhGk+/BeSTAVczZIZm1KNibbjZu+hXzkKQL5Enor0Q56M/8w7eOrLOW71ot9sPkSHBSig2duyoUV/kYJ33JNW34n0koP99GC/mcUvW+7bQZBC5VzOCOLsd0Y=');
    this.viewRef = React.createRef();
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    this.setupScanning();
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
    this.dataCaptureContext.dispose();
  }

  handleAppStateChange = async (nextAppState) => {
    if (nextAppState.match(/inactive|background/)) {
      this.stopCapture();
    } else {
      this.startCapture();
    }
  }

  startCapture() {
    this.startCamera();
    this.barcodeCaptureMode.isEnabled = true;
  }

  stopCapture() {
    this.barcodeCaptureMode.isEnabled = false;
    this.stopCamera();
  }

  stopCamera() {
    if (this.camera) {
      this.camera.switchToDesiredState(FrameSourceState.Off);
    }
  }

  startCamera() {
    if (!this.camera) {
      // Use the world-facing (back) camera and set it as the frame source of the context. The camera is off by
      // default and must be turned on to start streaming frames to the data capture context for recognition.
      this.camera = Camera.default;
      this.dataCaptureContext.setFrameSource(this.camera);

      const cameraSettings = new CameraSettings();
      cameraSettings.preferredResolution = VideoResolution.FullHD;
      this.camera.applySettings(cameraSettings);
    }

    // Switch camera on to start streaming frames and enable the barcode capture mode.
    // The camera is started asynchronously and will take some time to completely turn on.
    requestCameraPermissionsIfNeeded()
        .then(() => this.camera.switchToDesiredState(FrameSourceState.On))
        .catch(() => BackHandler.exitApp());
  }

  setupScanning() {
    // The barcode capturing process is configured through barcode capture settings
    // and are then applied to the barcode capture instance that manages barcode recognition.
    const settings = new BarcodeCaptureSettings();

    // The settings instance initially has all types of barcodes (symbologies) disabled. For the purpose of this
    // sample we enable a very generous set of symbologies. In your own app ensure that you only enable the
    // symbologies that your app requires as every additional enabled symbology has an impact on processing times.

    // commentato ES 19022021
    // settings.enableSymbologies([
    //   Symbology.EAN13UPCA,
    //   Symbology.EAN8,
    //   Symbology.UPCE,
    //   Symbology.QR,
    //   Symbology.DataMatrix,
    //   Symbology.Code39,
    //   Symbology.Code128,
    //   Symbology.InterleavedTwoOfFive,
    // ]);

    const ean13upcaSettings = settings.settingsForSymbology(Symbology.EAN13UPCA);
    ean13upcaSettings.isEnabled = true;
    ean13upcaSettings.setExtensionEnabled("two_digit_add_on", true);

    const ean8Settings = settings.settingsForSymbology(Symbology.EAN8);
    ean8Settings.isEnabled = true;
    ean8Settings.setExtensionEnabled("five_digit_add_on", true);


    // Some linear/1d barcode symbologies allow you to encode variable-length data. By default, the Scandit
    // Data Capture SDK only scans barcodes in a certain length range. If your application requires scanning of one
    // of these symbologies, and the length is falling outside the default range, you may need to adjust the "active
    // symbol counts" for this symbology. This is shown in the following few lines of code for one of the
    // variable-length symbologies.
    const symbologySettings = settings.settingsForSymbology(Symbology.Code39);
    symbologySettings.activeSymbolCounts = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

    // Create new barcode capture mode with the settings from above.
    this.barcodeCaptureMode = BarcodeCapture.forContext(this.dataCaptureContext, settings);

    // Register a listener to get informed whenever a new barcode got recognized.
    this.barcodeCaptureListener = {
      didScan: (_, session) => {
        const barcode = session.newlyRecognizedBarcodes[0];
        const symbology = new SymbologyDescription(barcode.symbology);

        // The `alert` call blocks execution until it's dismissed by the user. As no further frames would be processed
        // until the alert dialog is dismissed, we're showing the alert through a timeout and disabling the barcode
        // capture mode until the dialog is dismissed, as you should not block the BarcodeCaptureListener callbacks for
        // longer periods of time. See the documentation to learn more about this.
        this.barcodeCaptureMode.isEnabled = false;
        const addOnData = barcode.addOnData;

        Alert.alert(
            null,
            `Scanned: ${barcode.data} ${barcode.addOnData} (${symbology.readableName})`,
            [{ text: 'OK', onPress: () => this.barcodeCaptureMode.isEnabled = true }],
            { cancelable: false }
        );

        // Alert.alert(
        //   null,
        //   `Scanned: ${barcode.data} (${symbology.readableName})`,
        //   [{ text: 'OK', onPress: () => this.barcodeCaptureMode.isEnabled = true }],
        //   { cancelable: false }
        // );



      }
    };

    this.barcodeCaptureMode.addListener(this.barcodeCaptureListener);

    // Add a barcode capture overlay to the data capture view to render the location of captured barcodes on top of
    // the video preview. This is optional, but recommended for better visual feedback.
    this.overlay = BarcodeCaptureOverlay.withBarcodeCaptureForView(this.barcodeCaptureMode, this.viewRef.current);
    this.overlay.viewfinder = new RectangularViewfinder();
    this.overlay = this.overlay;
  }

  render() {
    return (
        // <View>


         <DataCaptureView style={{ flex: 1 }} context={this.dataCaptureContext} ref={this.viewRef} />
          // <DataCaptureView/>
        // </View>
    );
  };
}
